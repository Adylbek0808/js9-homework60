import React from 'react';
import './Message.css';
import moment from 'moment';

const Message = props => {
    return (
        <div className="post">
            <p className="postUser">
                <span className="postUserName"> {props.author} says:</span>
                <span className="postTime">({moment(props.time).format('MMMM Do YYYY, HH:mm:ss ')})</span>
            </p>
            <p className="postMessage">{props.message}</p>
        </div>
    );
};

export default Message;