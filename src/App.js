import React from 'react';
import './App.css';
import MessagesHomework from './containers/MessagesHomework/MessagesHomework';

function App() {
  return (
    <MessagesHomework />
  );
}

export default App;
